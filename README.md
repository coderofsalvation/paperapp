deadsimple json-based SPA app-generator for gitlab (static pages)

<img src="https://gitlab.com/coderofsalvation/paperapp/raw/master/doc/example1.gif" style="width:100%"/>

# Usage 

$ npm install paperapp
$ paperapp

 	paperapp init                     <-- inits a directory with a paper app
	paperapp dev                      <-- runs development server

# How to create a project

	$ npm init
	$ npm install paperapp --save
	$ paperapp init
	$ paperapp dev
	   _   _   _   _   _   _   _   _  
	  / \ / \ / \ / \ / \ / \ / \ / \ 
	 ( P | A | P | E | R | A | P | P )
	  \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ 

	  https://npmjs.org/package/paperapp

	Starting up http-server, serving .server
	Available on:
	  http://127.0.0.1:8080
	  http://100.115.92.1:8080
	  http://192.168.0.157:8080
	Hit CTRL-C to stop the server

> Now edit `app.json`,  `app.css` and `app.js` in your current dir

	$ git add .gitlab-ci.yml app.*
    $ git commit -m "1st commit"
    $ git push origin master

> Voila you have a beautiful offline SPA up and running

<img src="https://gitlab.com/coderofsalvation/paperapp/raw/master/doc/screenshot.png" style="width:100%"/>

# Features

* [Old device-friendly](doc/olddevices.md)
* [Offline SPA](doc/offline.md)
* [Reactive store](doc/store.md)
* SPA with SEO-support

# Reference

| Feature | Example | Result | Comment |
|-|-|-|-|
| *JSON Reactive store*     | `{ "store": { "name":"John" } }`     | | define default store variables |
|                     | `{ "name":  "${store.name}" } }`     | | use store-variable as value |
| *JSON js function*  | `{ "format":"div", "value":"${window.Date()}" }`     | | use function output as div-content |
| *JSON Webrequest*   | `{ "format":"div", "value":"${res/products.html}" }`     | | use url as div-content |
| 					  | `{ "$ref":"https://f.com/2.json" }` | | uses fetched json-result as value |
| *JS Reactive*       | `$.on('store.name', console.log )`   | | reacts to value-change |
|                     | `$.set('store.name', "myname")`      | | updates value |
| 				      | `$.set('store.name', $.store.name)`  | | retrigger last value |
| *Application Events*| `$.on('/init',        console.log )`     | | respond to event |
| 					  | `$.on('/init/done',   $.curry(console.log,"done!"))`     | | respond to event (curried) |
| 					  | `$.on('/init/done',   [alert,console.log])`     | | respond to event (auto map) |
| 					  | `$.on('/menu/change', console.log )`     | | respond to event |
| 					  | `$.trigger('/menu/change', {target:$('#menu select')})` | | trigger event |
| 					  | `$.off('/menu/change',console.log` | | unregister eventhandler |
| *JS Forms" 		  | `$.createForm( myjsonschema )` | | renders a jsonschema form |
| *JS DOM*			  | `$.Element({format:"li",value:"hi",attr:{"id":"foo"},style:{"color":"#FFF"}}),class:"foo"})` | <li id="foo" style="color:#FFF;">hi</li> | |
|                     | `$('div>li#a')` | HTMLElement | like jquery |
|                     | `$.all('div>li').map(console.dir)` | HTMLElement[] | like jquery |
|                     | `$.addClass( $('#foo'),'red')` | | |
|                     | `$.removeClass( $('#foo'),'red')` | | |
| *JS Utility*		  | `$.extend({a:1},{b:2})` | {a:1,b:1} | like lodash |
| 					  | `$.get($,'store.foo.bar','empty')` | value or 'empty' | shorthand function like lodash |
| 					  | `$.set($,'store.foo.bar',123)` |  | shorthand function like lodash  |
| 					  | `$.equals(property, a, b)` |  | example: myarr.find( equals('title', 'foo') )  |
| 					  | `foo = $.curry(console.log,"foo")` | foo() // prints 'foo' | lazy curry |
|                     | `$.renderString("hello ${world}",{world:"man"})` | "hello man" | es6 template evaluation to string |
|                     | `$.renderVar("${store})` | {...} | es6 template evaluation to object |
|                     | `$.mapasync([1,2,3],function(v,i,next){ next() }, alert)` |  | async iterator |
| *JS webrequest      | `$.require("https://foo.com/1.js","js", alert )` | <script src="https://foo.com/1.js"/> | require js or css file |
|                     | `$.request('PUT','http://f.com/1.json', console.dir)` | string with json | will return cached version if offline |
| url hash variables  | http://foo.com#{"nomenu":true} | | hides menu icon |
| 					  | http://foo.com#{"nofooter":true} | | hides footer |
| 					  | http://foo.com#{"noheader":true} | | hides header |
| 					  | http://foo.com#{"nofullscreen":true} | | hides fullscreen icon |
| 					  | http://foo.com#{"card":"Foo"} | | sets 'Foo' as homepage |




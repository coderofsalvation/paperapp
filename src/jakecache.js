'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _eventHandlers = Symbol('eventHandlers');

var CustomEvent = window.CustomEvent;
var DOMException = window.DOMException;
var ErrorEvent = window.ErrorEvent;
var ProgressEvent = window.ProgressEvent;

var PolyfilledEventTarget = function () {
  function PolyfilledEventTarget(names) {
    var _this = this;

    _classCallCheck(this, PolyfilledEventTarget);

    this[_eventHandlers] = {};

    names.map(function (name) {
      _this[_eventHandlers][name] = { handler: null, listeners: [] };
      Object.defineProperty(_this, 'on' + name, {
        get: function get() {
          return this[_eventHandlers][name]['handler'];
        },
        set: function set(fn) {
          if (fn === null || fn instanceof Function) {
            this[_eventHandlers][name]['handler'] = fn;
          }
        },
        enumerable: false
      });
    });
  }

  _createClass(PolyfilledEventTarget, [{
    key: 'dispatchEvent',
    value: function dispatchEvent(event) {
      if (this[_eventHandlers][event.type]) {
        var handlers = this[_eventHandlers][event.type];
        var mainFn = handlers['handler'];
        if (mainFn) {
          mainFn(event);
        }
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = handlers['listeners'][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var fn = _step.value;

            fn(event);
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      }
    }
  }, {
    key: 'addEventListener',
    value: function addEventListener(name, fn) {
      if (this[_eventHandlers][name]) {
        var store = this[_eventHandlers][name]['listeners'];
        var index = store.indexOf(fn);
        if (index === -1) {
          store.push(fn);
        }
      }
    }
  }, {
    key: 'removeEventListener',
    value: function removeEventListener(name, fn) {
      if (this[_eventHandlers][name]) {
        var store = this[_eventHandlers][name]['listeners'];
        var index = store.indexOf(fn);
        if (index > 0) {
          store.splice(index, 1);
        }
      }
    }
  }]);

  return PolyfilledEventTarget;
}();

var _status = Symbol('status');

var JakeCache = function (_PolyfilledEventTarge) {
  _inherits(JakeCache, _PolyfilledEventTarge);

  function JakeCache() {
    _classCallCheck(this, JakeCache);

    var _this2 = _possibleConstructorReturn(this, (JakeCache.__proto__ || Object.getPrototypeOf(JakeCache)).call(this, ['abort', 'cached', 'checking', 'downloading', 'error', 'obsolete', 'progress', 'updateready', 'noupdate']));

    if (window.jakeCache) {
      var _ret;

      return _ret = window.jakeCache, _possibleConstructorReturn(_this2, _ret);
    }
    window.jakeCache = _this2;

    if ('serviceWorker' in navigator === false) {
      return _possibleConstructorReturn(_this2);
    }

    var onload = function onload() {
      if (document.readyState !== 'complete') {
        return;
      }

      var html = document.querySelector('html');
      _this2.pathname = html.getAttribute('manifest');

      if (_this2.pathname && 'serviceWorker' in navigator) {
        navigator.serviceWorker.register('jakecache-sw.js').then(function (registration) {
          console.log('JakeCache installed for ' + registration.scope);

          if (registration.active) {
            // Check whether we have a cache, or cache it (no reload enforced).
            console.log('cache check');
            registration.active.postMessage({
              command: 'update',
              pathname: _this2.pathname
            });
          }
        }).catch(function (err) {
          console.log('JakeCache installation failed: ' + err);
        });
      }
    };

    if (document.readyState === 'complete') {
      onload();
    } else {
      document.onreadystatechange = onload;
    }

    _this2[_status] = _this2.UNCACHED;

    navigator.serviceWorker.addEventListener('message', function (event) {
      switch (event.data.type) {
        case 'abort':
          _this2.dispatchEvent(new CustomEvent('abort'));
          break;
        case 'idle':
          _this2[_status] = _this2.IDLE;
          break;
        case 'checking':
          _this2[_status] = _this2.CHECKING;
          _this2.dispatchEvent(new CustomEvent('checking'));
          break;
        case 'cached':
          _this2[_status] = _this2.IDLE;
          _this2.dispatchEvent(new CustomEvent('cached'));
          break;
        case 'downloading':
          _this2[_status] = _this2.DOWNLOADING;
          _this2.dispatchEvent(new CustomEvent('downloading'));
          break;
        case 'updateready':
          _this2[_status] = _this2.UPDATEREADY;
          _this2.dispatchEvent(new CustomEvent('updateready'));
          break;
        case 'noupdate':
          _this2[_status] = _this2.IDLE;
          _this2.dispatchEvent(new CustomEvent('noupdate'));
          break;
        case 'progress':
          _this2.dispatchEvent(new ProgressEvent('progress', event.data));
          break;
        case 'obsolete':
          _this2[_status] = _this2.OBSOLETE;
          _this2.dispatchEvent(new CustomEvent('obsolete'));
          break;
        case 'error':
          _this2.dispatchEvent(new ErrorEvent('error', event.data));
          break;
      }
    });
    return _this2;
  }

  _createClass(JakeCache, [{
    key: 'update',
    value: function update() {
      if (false) {}

      navigator.serviceWorker.controller.postMessage({
        command: 'update',
        pathname: this.pathname,
        options: {
          cache: 'reload'
        }
      });
    }
  }, {
    key: 'abort',
    value: function abort() {
      if (this.status === this.DOWNLOADING) {
        navigator.serviceWorker.controller.postMessage({
          command: 'abort'
        });
      }
    }
  }, {
    key: 'swapCache',
    value: function swapCache() {
      if (this.status !== this.UPDATEREADY) {
        throw new DOMException(DOMException.INVALID_STATE_ERR, 'there is no newer application cache to swap to.');
      }
      navigator.serviceWorker.controller.postMessage({
        command: 'swapCache'
      });
    }
  }, {
    key: 'UNCACHED',
    get: function get() {
      return 0;
    }
  }, {
    key: 'IDLE',
    get: function get() {
      return 1;
    }
  }, {
    key: 'CHECKING',
    get: function get() {
      return 2;
    }
  }, {
    key: 'DOWNLOADING',
    get: function get() {
      return 3;
    }
  }, {
    key: 'UPDATEREADY',
    get: function get() {
      return 4;
    }
  }, {
    key: 'OBSOLETE',
    get: function get() {
      return 5;
    }
  }, {
    key: 'status',
    get: function get() {
      return this[_status];
    }
  }]);

  return JakeCache;
}(PolyfilledEventTarget);

new JakeCache();

$.sub('/init', 			[console.dir] )
$.sub('/init/done', 	[console.dir] )
$.sub('/menu/change', 	[console.dir] )

$.sub('/route/change',  function(e){
  if( e.hidden ) return $.showCard( 'Not in menu' ) // triggered by index.html?hidden=true
  $.pub('/menu/change', {target:$('#menu select')}) // default routing behaviour 
})

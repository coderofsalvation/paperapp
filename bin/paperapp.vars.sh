logo='   _   _   _   _   _   _   _   _  
  / \ / \ / \ / \ / \ / \ / \ / \ 
 ( P | A | P | E | R | A | P | P )
  \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ 

  https://npmjs.org/package/paperapp
'

usage='
	paperapp init                     <-- inits a directory with a paper app
	paperapp dev                      <-- runs development server
	paperapp production               <-- emulates production server (generated html)
	paperapp deploy <outdir>          <-- generates runtime files (to host on server)

'

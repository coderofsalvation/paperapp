var fs = require('fs')
var fetch = require('node-fetch')
var _ = require('lodash')

/* load src/paperapp.js */
var browserjs = fs.readFileSync( __dirname+"/../src/paperapp.js").toString()
var dummy = function(){}
var document = {
	querySelector:dummy,  
	querySelectorAll:dummy
}
var addEventlistener = dummy
var navigator = dummy
var window = { document, addEventListener: dummy }
eval(browserjs)
var $ = new window.paperapp() 


$.build = function(){
	var dir = process.argv[3]
	process.chdir(dir)
	var app = require( process.cwd() + '/app.json')
	app.cards.map($.cardToHTML.bind($,app))

}

$.fetchResource = function(c, app){
	return new Promise( (resolve, reject) => {
		fetch(c)
		.then( (res) => res.text() )
		.then( (res) => {
			var file = 'res/component/' + c.split("/").pop().replace(/(\?.*|#.*)/, '')
			console.log("writing "+file)
			fs.writeFileSync( file,  res )
			for ( var i in app.components ) app.components[i] = app.components[i] == c ? file : app.components[i]
			resolve()
		})
		.catch(reject)
	})
}

$.fetchResources = function(){
	var dir = process.argv[3]
	process.chdir(dir)
	var app = require( process.cwd() + '/app.json')
	var p = []
	app.components.map( function(c){
		if( !c.match(/^http/) ) return		
		p.push( $.fetchResource(c, app) )
	})
	Promise.all(p)
	.then( () => {
		fs.writeFileSync( process.cwd() + '/app.json', JSON.stringify(app, null, 2) )
		console.log("writing app.json")
	})
}

$.generateIndex = function(){
	var dir = process.argv[3]
	process.chdir(dir)
	console.log("writing index.html")
	var app = require( process.cwd() + '/app.json')
	var html = fs.readFileSync( process.cwd()+"/index.html" ).toString()
	var menu = '<ul>${links}</ul>' 
	var searchstr = '<div class="wrapper">'
	var links = app.cards.map( function(c){
		return '<li style="color:#FFF"><a style="color:#FFF" href="'+$.toSlug(c.title)+'.html">'+c.title+'</a></li>'			
	}).join('\n')
	html = html.replace(searchstr, searchstr+$.renderString(menu, {links} ) )
	fs.writeFileSync( process.cwd()+'/index.html', html )
}

$.cardToHTML = function(app, card){
	var html = fs.readFileSync( process.cwd()+"/index.html" ).toString()
	var file = process.cwd()+'/'+(card.prefix||'')+$.toSlug(card.title)+'.html' 
	var body = ''
	var content = '<div class="card" style="display:block">${body}</div>'
	_.concat( app.header||[], card.items, app.footer||[]).map( (i) => {
		if( !i.value ) return
		var tag = i.format||'div'
		var isFile = i.value.match(/\$\{.*[\/].*\}$/) && !i.value.match(/\$\{http/)
		if( isFile ){ 
          i.value = require('fs').readFileSync( process.cwd()+'/'+i.value.replace(/(\$\{|\})/g,'') ).toString()
		  body += '<'+tag+'>'+i.value+'</'+tag+">\n"
        }
	})
	console.log("writing "+file)
	var searchstr = '<div class="wrapper">'
	html = html.replace(searchstr, searchstr+ $.renderString(content, {body}) )
	fs.writeFileSync( file, html )
}

$.cardsToHTML = function(){
	var json = process.argv[3]
	var outdir = process.argv[4]
	var app = require( json )
	app.cards.map( (c) => $.cardToHTML(app, c)  )
}


var fn = process.argv[2]
$[fn]()

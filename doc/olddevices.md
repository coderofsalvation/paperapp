# Old devices supported

To solve these problems:

* my grandmothers smartphone can't simply display a React/Vuejs site
* crosswalk android applications are HUGE in size
* android native webview does not support the latest and greatest

These project-guidelines have been made:

* Ultra-backwards compatible code (written in 4k lines of ES5-code)
* use select as hamburger menu (always works)
* no virtual dom for the sake of store-reactivity (a pubsub suffices in 99% of the cases)

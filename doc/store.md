# Reactive Store 

A supersimple pubsub mechanism provides reactivity,  without introducing vdom:

# Store variables

	$.on('store.name', console.log )   // react to value-change
	$.set('store.name', "myname")      // update value
	$.set('store.name', $.store.name)  // retrigger last value 

> NOTE: replace `console.log` with your own function(){}

Which can be rendered in json or html with es6-handlebars:

	${store.myname}	

# Application state changes 

	$.on('/init', 			console.dir )
	$.on('/init/done', 	    console.dir )
	$.on('/menu/change', 	console.dir )

> NOTE: replace `console.dir` with your own function

# Custom state changes

	$.on('/myevent',      console.dir )        // react to event
    $.trigger('/myevent', "hello world!" )     // trigger event!
    $.off('/myevent,  console.dir ) 		   // stop reacting

